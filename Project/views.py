from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_object_or_404, render, render_to_response


def index_view(request):
    """
    Render Vue.js index.html
    """
    return render(request, 'index.html', {'title': _('Hello World!')})
