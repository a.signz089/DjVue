"""Project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from django.views.generic import RedirectView, TemplateView
from rest_framework import routers

from Project.views import index_view
from API import views as API

router = routers.DefaultRouter()
router.register(r'users', API.UserViewSet)

favicon_view = RedirectView.as_view(url=settings.STATIC_URL + 'favicon.ico', permanent=True)

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$', index_view, name='index'),
    url(r'^api/', include(router.urls)),
    url(r'^favicon\.ico$', favicon_view),
    url(r'^robots.txt$', TemplateView.as_view(template_name="robots.txt", content_type="text/plain"), name="robots_txt")
]
