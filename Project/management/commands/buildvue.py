import os, subprocess
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Build Vue.js app'

    def handle(self, *args, **options):
        print(help, '...')
        print('npm install')
        subprocess.check_output('npm install', shell=True)
        print('npm run build')
        subprocess.check_output('npm run build', shell=True)
