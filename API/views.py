from django.shortcuts import render, get_object_or_404
from django.utils.translation import ugettext as _
from django.conf import settings
from rest_framework import viewsets
from django.utils import timezone
from rest_framework import permissions
from rest_framework import generics, viewsets
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response

from .permissions import IsOwner, IsOwnerOrReadOnly, IsSelf
from .serializers import UserSerializer

from Project.models import User


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

    @list_route(methods=['get'], permission_classes=[permissions.IsAdminUser])
    def recent_users(self, request):
        recent_users = User.objects.all().order('-last_login')

        page = self.paginate_queryset(recent_users)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(recent_users, many=True)
        return Response(serializer.data)

    @detail_route(methods=['post', 'patch'], permission_classes=[IsSelf, permissions.IsAdminUser])
    def retrieve_user(self, request, pk=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = UserSerializer(user, context={'request': request})
        return Response(serializer.data)
