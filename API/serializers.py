from django.urls import reverse
from rest_framework import serializers, exceptions
from rest_framework.exceptions import ValidationError

from Project.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'first_name', 'last_name')
