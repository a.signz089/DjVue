# Dj.Vue

> A Django Vue.js project

## Install

### Django
setup venv with ACL
``` bash
export VENV_ROOT=.venv
sudo mkdir ${VENV_ROOT}
sudo setfacl -m "u:asignz:rwx" ${VENV_ROOT}
sudo setfacl -m "u:http:--x" ${VENV_ROOT}
sudo setfacl -dm "u:asignz:rwx" ${VENV_ROOT}
sudo setfacl -dm "u:http:r-x" ${VENV_ROOT}
sudo chmod o-rx ${VENV_ROOT}

python3 -m venv ${VENV_ROOT}

. ${VENV_ROOT}/bin/activate
```
install Django to venv

``` bash
pip install django-heroku
# or from requirements.txt
pip install -r requirements.txt

pip freeze > requirements.txt
```

REST
```bash
pip install django-cors-headers django-rest-framework
```
social auth
```bash
pip install django-allauth
```

## Vue.js
### Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Deploy
development
```bash
# ENV
export DJANGO_DEBUG=1
export DATABASE_URL=postgres:///{ Project DB }

# Django
./manage.py runserver 0.0.0.0:8080

# Vue
# Build for Django
npm run build
# Run with node
npm run dev
```

### Heroku
```bash
heroku create { app-name } \
 --buildpack https://github.com/Julz089/heroku-buildpack-python#Vue.js

heroku config:set { key }={ value }

heroku addons:add heroku-postgresql
```

##### © [a.Signz] 2018 #####
[a.Signz]: https://asignz.com
